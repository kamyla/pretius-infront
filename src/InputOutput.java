import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InputOutput {

	private FileReader fileReader;
	private double totalAmount;

	public InputOutput(String path) throws FileNotFoundException {
		fileReader = new FileReader(path);
		totalAmount = 0;
	}

	public void readLine() throws IOException {
		BufferedReader bufferedReader = new BufferedReader(fileReader);

		String textLine = bufferedReader.readLine();
		do {
			totalAmount += getAmount(textLine);

			textLine = bufferedReader.readLine();
		} while(textLine != null);

		java.text.DecimalFormat df = new java.text.DecimalFormat();
		
		df.setMaximumFractionDigits(2);
		df.setMinimumFractionDigits(2);
		
		System.out.println("Suma przelew�w: " + df.format(totalAmount) + " PLN");
		
		bufferedReader.close();
	}

	private double getAmount(String line) {
		Pattern p = Pattern.compile("@amount:");
		Matcher m = p.matcher(line);
		
		if(m.find()){
			line = line.replace(',', '.');
			return Double.parseDouble(line.substring(m.end(),line.length()-3)); //zak�adam, �e zawsze PLN
		}
		return 0;
	}
}
