import java.io.IOException;

public class Main {
	public static void main(String[] args) {

		try {
			InputOutput io = new InputOutput(args[0]);
			io.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
